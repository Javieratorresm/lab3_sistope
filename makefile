DIR_OUTPUT = ./build
DIR_SRC = ./src
NOMBRE_EJECUTABLE = programa1
NOMBRE_EJECUTABLE2=programa2
# Esta es la target que se ejecuta por defecto si se escribe "make" en la consola
all: release

# Compilación principal del ejecutable
release:
	@echo 'Compilando programa 1'
	mkdir -p $(DIR_OUTPUT)
	gcc $(DIR_SRC)/programa1.c -o $(DIR_OUTPUT)/$(NOMBRE_EJECUTABLE)
	@echo ' '
	@echo "Ingrese al directorio '"$(DIR_OUTPUT)"' haciendo: cd "$(DIR_OUTPUT)
	@echo "Ejecute el programa haciendo: ./"$(NOMBRE_EJECUTABLE)
	@echo ' '
	@echo 'Compilando programa 2'
	mkdir -p $(DIR_OUTPUT)
	gcc $(DIR_SRC)/programa2.c -o $(DIR_OUTPUT)/$(NOMBRE_EJECUTABLE2)
	@echo ' '
	@echo "Ingrese al directorio '"$(DIR_OUTPUT)"' haciendo: cd "$(DIR_OUTPUT)
	@echo "Ejecute el programa haciendo: ./"$(NOMBRE_EJECUTABLE2)
	@echo ' '
	@echo "Para mas información, lea el archivo README."